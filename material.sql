

-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.6.26 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win32
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 jfinal_demo.material 结构
CREATE TABLE IF NOT EXISTS `material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 正在导出表  jfinal_demo.material 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `material` DISABLE KEYS */;
INSERT INTO `material` (`id`, `name`) VALUES
	(1, '机器人'),
	(2, '机床');
/*!40000 ALTER TABLE `material` ENABLE KEYS */;

-- 导出  表 jfinal_demo.materialinfo 结构
CREATE TABLE IF NOT EXISTS `materialinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materialid` int(11) NOT NULL,
  `quantiy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 正在导出表  jfinal_demo.materialinfo 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `materialinfo` DISABLE KEYS */;
INSERT INTO `materialinfo` (`id`, `materialid`, `quantiy`) VALUES
	(1, 1, 100),
	(2, 2, 90);
/*!40000 ALTER TABLE `materialinfo` ENABLE KEYS */;

-- 导出  表 jfinal_demo.materialpart 结构
CREATE TABLE IF NOT EXISTS `materialpart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentid` int(11) NOT NULL,
  `detail` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- 正在导出表  jfinal_demo.materialpart 的数据：~4 rows (大约)
/*!40000 ALTER TABLE `materialpart` DISABLE KEYS */;
INSERT INTO `materialpart` (`id`, `parentid`, `detail`) VALUES
	(1, 1, '部件A'),
	(2, 1, '部件B'),
	(3, 2, '部件C'),
	(4, 2, '部件D');
/*!40000 ALTER TABLE `materialpart` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;



修改说明:
1._JFinalDemoGenerator 中 GeneratorEx generator = new GeneratorEx，然后按格式配置外键关系setFK
2.扩展类  ColumnMetaEx， MetaBuilderEx， BaseModelGeneratorEx ，修改模版 base_model_template_fk.jf
3.更深入的 应用 ，如save，update，delete时自动更新从表， 由于原先设计的一些限制，需要波总统一考量框架的适用性
 
 