# jfinal_fk

#### 项目介绍
在jfinal的基础上，自动生成级连属性，
如

1:N  主表 material.id  分录表 materialpart.parentid 

1:1  主表 material.id  扩展表 materialinfo.materialid  


会自动生成级连的下级属性 ， 如：
 

public abstract class BaseMaterial<M extends BaseMaterial<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}
	
	
	public List<Materialpart> getParts(String fields){
		if(_getAttrs().containsKey("parts")){
			return get("parts"); 
		}else{
			List<Materialpart> val = new Materialpart().dao().find("select " + fields + " from materialpart where parentid = ? ", get("id"));
			_getAttrs().put("parts", val);
			return  val;
		}
	}
	
	public List<Materialpart> getParts(){
		return getParts("*");
	}
	
			
			
	public Materialinfo getMaterialinfo(String fields){
		if(_getAttrs().containsKey("materialinfo")){
			return get("materialinfo"); 
		}else{
			Materialinfo val = new Materialinfo().dao().findFirst("select " + fields + " from materialinfo where materialid = ? ", get("id"));
			_getAttrs().put("materialinfo", val);
			return  val;
		}
	}
	
	public Materialinfo getMaterialinfo(){
		return getMaterialinfo("*");
	}
	

	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}
	
	

}



public abstract class BaseMaterialinfo<M extends BaseMaterialinfo<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}
	
	

	public void setMaterialid(java.lang.Integer materialid) {
		set("materialid", materialid);
	}
	
	public java.lang.Integer getMaterialid() {
		return getInt("materialid");
	}
	
    public Material getMaterial(String fields){
    	if(_getAttrs().containsKey("material")){
			return get("material"); 
		}else{
			Material val = new Material().dao().findFirst("select " + fields + " from material where  id = ? limit 1 ", get("materialid"));
			_getAttrs().put("material", val);
			return  val;
		} 
  	}
  	
  	public Material getMaterial(){
  		return getMaterial("*");
  	}
	

	public void setQuantiy(java.lang.Integer quantiy) {
		set("quantiy", quantiy);
	}
	
	public java.lang.Integer getQuantiy() {
		return getInt("quantiy");
	}
	
	

}


修改说明:
以下几个文件拷贝到适当package下
ColumnMetaEx.java  
FK.java  
FKEngine.java 
MetaBuilderEx.java
base_model_template_fk.jf

然后改写生成model的代码
_JFinalDemoGenerator.java
 

// 创建生成器 

Generator generator = new Generator(ds, baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
		
//自动FK 需要改动的地方begin

generator.setMetaBuilder(new MetaBuilderEx(ds));

generator.setBaseModelTemplate("/com/demo/common/model/base_model_template_fk.jf");

FKEngine.engine.initFKs();

//自动FK 需要改动的地方end


initFKs方法里写入 链接关系，如：

addFK("material.id  material  -1:N- materialpart.parentid  parts");

addFK("material.id   -1:1- materialinfo.materialid ");

就能自动生成上诉的代码