package com.demo.common.model;

import java.util.List;

import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.jfinal.plugin.activerecord.generator.TableMeta;

public class ColumnMetaEx extends ColumnMeta {
	public boolean isMasterFK = false; 
	//从表有0个或多个, list 中的i是一一对应的
	public List<TableMeta> slaveTables;
	public List<ColumnMetaEx> slaveFields;
	public List<FK> slaveFK;

	public boolean isSlaveFK = false;
	//主表只有1个
	public ColumnMetaEx masterField;
	public TableMeta masterTable;
	public FK masterFK;
	 

}
